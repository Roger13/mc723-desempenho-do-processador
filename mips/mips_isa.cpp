/**
 * @file      mips_isa.cpp
 * @author    Sandro Rigo
 *            Marcus Bartholomeu
 *            Alexandro Baldassin (acasm information)
 *
 *            The ArchC Team
 *            http://www.archc.org/
 *
 *            Computer Systems Laboratory (LSC)
 *            IC-UNICAMP
 *            http://www.lsc.ic.unicamp.br/
 *
 * @version   1.0
 * @date      Mon, 19 Jun 2006 15:50:52 -0300
 *
 * @brief     The ArchC i8051 functional model.
 *
 * @attention Copyright (C) 2002-2006 --- The ArchC Team
 *
 */

#include  "mips_isa.H"
#include  "mips_isa_init.cpp"
#include  "mips_bhv_macros.H"


//If you want debug information for this model, uncomment next line
//#define DEBUG_MODEL
#include "ac_debug_model.H"

#include <iostream>
#include <fstream>
#include <iomanip>

//!User defined macros to reference registers.
#define Ra 31
#define Sp 29

// 'using namespace' statement to allow access to all
// mips-specific datatypes
using namespace mips_parms;

static int processors_started = 0;

#define DEFAULT_STACK_SIZE (256*1024)

/*====================================================================================================
Variaveis para Projeto 2 MC723
======================================================================================================*/

// Trace para o simulador de cache
ofstream trace;

//Number of instructions fetched if the wrong branch decision was made.
#define branchPenalty5stages 2
#define branchPenalty8stages 3
#define branchPenalty13stages 13
typedef enum {takenZero, takenOne, notTakenZero, notTakenOne} SecondBranchPolicyState;
typedef enum {taken, notTaken} FirstBranchPolicyState;

FirstBranchPolicyState firstState = taken;
unsigned firstBranchErrors = 0;
SecondBranchPolicyState secondState = takenZero;
unsigned secondBranchErrors = 0;

unsigned numeroInstrucoes;
unsigned dataHazards [3]; //Número de hazards de dados para pipeline de 5, 8 e 14 estagios respectivamente
//Contadores de instrucoes
unsigned instTipoR = 0;
unsigned instTipoI = 0;
unsigned instrucoesParalelas=0;
string lastInstruction = "first";

unsigned instTipoJ = 0;
unsigned instTipoBranch = 0;
unsigned instTipoLoad = 0;
unsigned instTipoStore = 0;

unsigned bubbles5 = 0;
unsigned bubbles8 = 0;
unsigned bubbles14 = 0;

bool bubble [3]; //Booleano que avisa existencia de hazards para pipeline de 5, 8 e 14 estagios respectivamente
bool parallel;
typedef struct instrucao{
	unsigned addressRs;
	unsigned addressRt;
	unsigned addressRd;
	string op;
} instrucao;


std::vector<instrucao> histInstrucoes;
std::vector<instrucao> histInstrucoes8;
std::vector<instrucao> histInstrucoes14;

/*====================================================================================================
Funções para Projeto 2 MC723
======================================================================================================*/

//Grava instrução no fim da fila
void gravaInstrucao (string op, int pipeline, unsigned addressRs, unsigned addressRt, unsigned addressRd){
  instrucao* inst = new instrucao();
  inst->addressRs = addressRs;
  inst->addressRt = addressRt;
  inst->addressRd = addressRd; //contem o immediate ou address em operacoes not R-type
  inst->op = op;
  switch(pipeline){
	default:	
	case 5:
	  histInstrucoes.push_back(*inst);
	  if(histInstrucoes.size() > 5){ //limita tamanho do historico
		histInstrucoes.erase(histInstrucoes.begin());
	  }
	if(pipeline == 5){
		return;	
	}
	case 8:
	  histInstrucoes8.push_back(*inst);
	  if(histInstrucoes8.size() > 8){ //limita tamanho do historico
		histInstrucoes8.erase(histInstrucoes8.begin());
	  }
	if(pipeline == 8){
		return;	
	}
	case 14:
	  histInstrucoes14.push_back(*inst);
	  if(histInstrucoes14.size() > 14){ //limita tamanho do historico
		histInstrucoes14.erase(histInstrucoes14.begin());
	  }
  }
};

//Procura uma instrucao de load no pipeline que escreverá no endereço de leitura passado
//Retorna o indice da instrução se existir, caso contrário retorna -1
//(Assumindo Fowarding, hazard de dados apenas em situações Load-Use)
int procuraInstrucao (unsigned Read, int index, int pipeline){
  if(Read == -1){ //Caso instrução não leia
	return -1;
  }
  switch(pipeline){
	case 5:
	  for (int i = index; i >= 0; i--){
	    //Ve se algum load escreve no registrador lido
	    if(histInstrucoes[i].op == "load" && histInstrucoes[i].addressRt == Read){
	    	  return i;
	    }
	    //Ve se alguma opera��o do tipo R ou store escreve no registrador lido
	    if((histInstrucoes[i].op == "R-type" || histInstrucoes[i].op == "store") && histInstrucoes[i].addressRd == Read){
		  if(histInstrucoes.size() - i - 1 == 1){
			 bubble[0] = true;
		  }
		  continue;
	    }
	    //Ve se operacao aritmetica do tipo I escreve no registrador lido
	    if(histInstrucoes[i].op == "I-type" && histInstrucoes[i].addressRt == Read){
	    	  if(histInstrucoes.size() - i - 1 == 1){
			 bubble[0] = true;
		  }
		  continue;
	    }
	  }
	  return -1;
	case 8:
	  for (int i = index; i >= 0; i--){
	    //Ve se algum load escreve no registrador lido
	    if(histInstrucoes8[i].op == "load" && histInstrucoes8[i].addressRt == Read){
	    	  return i;
	    }
	    //Ve se alguma opera��o do tipo R ou store escreve no registrador lido
	    if((histInstrucoes8[i].op == "R-type" || histInstrucoes8[i].op == "store") && histInstrucoes8[i].addressRd == Read){
		  if(histInstrucoes8.size() - i - 1 == 1){
			 bubble[1] = true;
		  }
		  continue;
	    }
	    //Ve se operacao aritmetica do tipo I escreve no registrador lido
	    if(histInstrucoes8[i].op == "I-type" && histInstrucoes8[i].addressRt == Read){
	    	  if(histInstrucoes8.size() - i - 1 == 1){
			 bubble[1] = true;
		  }
		  continue;
	    }
	  }
	  return -1;
        case 14:
	  for (int i = index; i >= 0; i--){
	    //Ve se algum load escreve no registrador lido
	    if(histInstrucoes14[i].op == "load" && histInstrucoes14[i].addressRt == Read){
	    	  return i;
	    }
	    //Ve se alguma opera��o do tipo R ou store escreve no registrador lido
	    if((histInstrucoes14[i].op == "R-type" || histInstrucoes14[i].op == "store") && histInstrucoes14[i].addressRd == Read){
		  if(histInstrucoes14.size() - i - 1 == 1){
			 bubble[2] = true;
		  }
		  continue;
	    }
	    //Ve se operacao aritmetica do tipo I escreve no registrador lido
	    if(histInstrucoes14[i].op == "I-type" && histInstrucoes14[i].addressRt == Read){
	    	  if(histInstrucoes14.size() - i - 1 == 1){
			 bubble[2] = true;
		  }
		  continue;
	    }
	  }
	  return -1;
  }
  
};

void checaHazard (){
  //reseta o bubble
  bubble[0] = false;// 5
  bubble[1] = false;// 7
  bubble[2] = false;// 13

  int last, index, dist = 0;	
  if(histInstrucoes.size() < 1){ //caso historico so tenha 1 instrucao
     return;
  }
  if(histInstrucoes[last].op == "R-type") { //caso nova instrucao seja uma instrucao tipo R
	//hazard no pipeline de 5 estagios        
	//procura por load que escrevera no RS (lido pela nova instrucao)
	last = histInstrucoes.size() - 1;        
	index = procuraInstrucao(histInstrucoes[last].addressRs,last-1,5);
        if(index != -1){
            dist = last - index; //calcula distancia entre as instrucoes
            if(dist == 1) { 
                dataHazards[0]++;
		bubbles5++;
                bubble[0] = true;
		gravaInstrucao("R-type",5,-1,-1,-1); //Insere nops no pipe-line
            }
	}else{
		//procura por load que escrevera no RT (lido pela nova instrucao)
		last = histInstrucoes.size() - 1;        
		index = procuraInstrucao(histInstrucoes[last].addressRt,last-1,5);
		if(index != -1){
		    dist = last - index; //calcula distancia entre as instrucoes
		    if(dist == 1) { 
		        dataHazards[0]++;
			bubbles5++;
		        bubble[0] = true;
			gravaInstrucao("R-type",5,-1,-1,-1); //Insere nops no pipe-line

		    }
		}
	}
        //hazard no pipeline de 8 estagios
 	//procura por load que escrevera no RS (lido pela nova instrucao)
	last = histInstrucoes8.size() - 1;
        index = procuraInstrucao(histInstrucoes8[last].addressRs,last-1,8);
        if(index != -1){
            dist = last - index; //calcula distancia entre as instrucoes
            if(dist <= 2) { 
                dataHazards[1]++;
                bubble[1] = true;
		for(int i = dist; i <= 2; i++){
		   bubbles8++;
		   gravaInstrucao("R-type",8,-1,-1,-1); //Insere nops no pipe-line
		}
            }
        }else{//procura por load que escrevera no RT (lido pela nova instrucao)
		last = histInstrucoes8.size() - 1;	
		index = procuraInstrucao(histInstrucoes8[last].addressRt,last-1,8);
		if(index != -1){
		    dist = last - index; //calcula distancia entre as instrucoes
		    if(dist <= 2) { 
		        dataHazards[1]++;
		        bubble[1] = true;
		        for(int i = dist; i <= 2; i++){
			   bubbles8++;
			   gravaInstrucao("R-type",8,-1,-1,-1); //Insere nops no pipe-line
			}

		    }
		}
	}

        //hazard no pipeline de 14 estagios
	//procura por load que escrevera no RS (lido pela nova instrucao)
	last = histInstrucoes14.size() - 1;
        index = procuraInstrucao(histInstrucoes14[last].addressRs,last-1,14);
        if(index != -1){
            dist = last - index; //calcula distancia entre as instrucoes
            if(dist <= 3) { 
                dataHazards[2]++;
                bubble[2] = true;
		for(int i = dist; i <= 3; i++){
		   bubbles14++;	
		   gravaInstrucao("R-type",14,-1,-1,-1); //Insere nops no pipe-line
		}
            }
        }else{//procura por load que escrevera no RT (lido pela nova instrucao)
		last = histInstrucoes14.size() - 1;	
		index = procuraInstrucao(histInstrucoes14[last].addressRt,last-1,14);
		if(index != -1){
		    dist = last - index; //calcula distancia entre as instrucoes
		    if(dist <= 3) { 
		        dataHazards[2]++;
		        bubble[2] = true;
		        for(int i = dist; i <= 3; i++){
			   bubbles14++;	
			   gravaInstrucao("R-type",14,-1,-1,-1); //Insere nops no pipe-line
			}

		    }
		}
	}

  }else if(histInstrucoes[last].op == "I-type") { //caso nova instrucao seja uma instrucao tipo I
        //hazard no pipeline de 5 estagios
	//procura por load que escrevera no RS (lido pela nova instrucao)
	last = histInstrucoes.size() - 1; 
        index = procuraInstrucao(histInstrucoes[last].addressRs,last-1,5);
        if(index != -1){
            dist = last - index; //calcula distancia entre as instrucoes
            if(dist == 1) { 
                dataHazards[0]++;
                bubble[0] = true;
		bubbles5++;	
		gravaInstrucao("R-type",5,-1,-1,-1); //Insere nops no pipe-line

            }
	}
	//hazard no pipeline de 8 estagios
	//procura por load que escrevera no RS (lido pela nova instrucao)
        last = histInstrucoes8.size() - 1;
	index = procuraInstrucao(histInstrucoes8[last].addressRs,last-1,8);
        if(index != -1){
	    dist = last - index; //calcula distancia entre as instrucoes
            if(dist <= 2) { 
                dataHazards[1]++;
                bubble[1] = true;
                for(int i = dist; i <= 2; i++){
		   bubbles8++;	
		   gravaInstrucao("R-type",8,-1,-1,-1); //Insere nops no pipe-line
		}

            }
        }
        //hazard no pipeline de 14 estagios
	//procura por load que escrevera no RS (lido pela nova instrucao)
        last = histInstrucoes14.size() - 1;
	index = procuraInstrucao(histInstrucoes14[last].addressRs,last-1,14);
        if(index != -1){
	    dist = last - index; //calcula distancia entre as instrucoes
            if(dist <= 3) { 
                dataHazards[2]++;
                bubble[2] = true;
                for(int i = dist; i <= 3; i++){
		   bubbles14++;	
		   gravaInstrucao("R-type",14,-1,-1,-1); //Insere nops no pipe-line
		}

            }
        }


  }else if(histInstrucoes[last].op == "load") { //caso nova instrucao seja uma instrucao tipo load
	//hazard no pipeline de 5 estagios         
	//procura por load que escrevera no RD (lido pela nova instrucao)
	last = histInstrucoes.size() - 1;         
	index = procuraInstrucao(histInstrucoes[last].addressRd,last-1,5);
        if(index != -1){
            dist = last - index; //calcula distancia entre as instrucoes
            if(dist == 1) { 
		bubbles5++;	
                dataHazards[0]++;
                bubble[0] = true;
		gravaInstrucao("R-type",5,-1,-1,-1); //Insere nops no pipe-line

            }
	}
        //hazard no pipeline de 8 estagios
	//procura por load que escrevera no RD (lido pela nova instrucao)
        last = histInstrucoes8.size() - 1;	
	index = procuraInstrucao(histInstrucoes8[last].addressRd,last-1,8);
        if(index != -1){
	    dist = last - index; //calcula distancia entre as instrucoes
            if(dist <= 2) { 

                dataHazards[1]++;
                bubble[1] = true;
                for(int i = dist; i <= 2; i++){
		   bubbles8++;	
		   gravaInstrucao("R-type",8,-1,-1,-1); //Insere nops no pipe-line
		}

            }
        }

        //hazard no pipeline de 13 estagios
	//procura por load que escrevera no RD (lido pela nova instrucao)
        last = histInstrucoes14.size() - 1;	
	index = procuraInstrucao(histInstrucoes14[last].addressRd,last-1,14);
        if(index != -1){
	    dist = last - index; //calcula distancia entre as instrucoes
            if(dist <= 3) { 
	
                dataHazards[2]++;
                bubble[2] = true;
                for(int i = dist; i <= 3; i++){
		   bubbles14++;	
		   gravaInstrucao("R-type",14,-1,-1,-1); //Insere nops no pipe-line
		}

            }
        }


  }else if(histInstrucoes[last].op == "store") { //caso nova instrucao seja uma instrucao tipo store
        //hazard no pipeline de 5 estagios
	//procura por load que escrevera no RS (lido pela nova instrucao)
	last = histInstrucoes.size() - 1;         
	index = procuraInstrucao(histInstrucoes[last].addressRs,last-1,5);
        if(index != -1){
            dist = last - index; //calcula distancia entre as instrucoes
            if(dist == 1) { 
                dataHazards[0]++;
                bubble[0] = true;
		bubbles5++;	
		gravaInstrucao("R-type",5,-1,-1,-1); //Insere nops no pipe-line
            }
	}
        //hazard no pipeline de 8 estagios
	//procura por load que escrevera no RS (lido pela nova instrucao)
    	last = histInstrucoes8.size() - 1;	
	index = procuraInstrucao(histInstrucoes8[last].addressRs,last-1,8);
        if(index != -1){
	    dist = last - index; //calcula distancia entre as instrucoes
            if(dist <= 2) { 
                dataHazards[1]++;
                bubble[1] = true;
                for(int i = dist; i <= 2; i++){
		   bubbles8++;	
		   gravaInstrucao("R-type",8,-1,-1,-1); //Insere nops no pipe-line
		}
            }
        }
	//hazard no pipeline de 13 estagios
	//procura por load que escrevera no RS (lido pela nova instrucao)
    	last = histInstrucoes14.size() - 1;	
	index = procuraInstrucao(histInstrucoes14[last].addressRs,last-1,14);
        if(index != -1){
	    dist = last - index; //calcula distancia entre as instrucoes
            if(dist <= 3) { 
                dataHazards[2]++;
                bubble[2] = true;
                for(int i = dist; i <= 3; i++){
		   bubbles14++;	
		   gravaInstrucao("R-type",14,-1,-1,-1); //Insere nops no pipe-line
		}
            }
        }

  }else if(histInstrucoes[last].op == "branch") { //caso nova instrucao seja uma instrucao tipo R
	//hazard no pipeline de 5 estagios        
	//procura por load que escrevera no RS (lido pela nova instrucao)
	last = histInstrucoes.size() - 1;         
	index = procuraInstrucao(histInstrucoes[last].addressRs,last-1,5);
        if(index != -1){
            dist = last - index; //calcula distancia entre as instrucoes
            if(dist == 1) { 
                dataHazards[0]++;
                bubble[0] = true;
		bubbles5++;	
		gravaInstrucao("R-type",5,-1,-1,-1); //Insere nops no pipe-line

                return; //retorna no primeiro hazard
            }
	}else{//procura por load que escrevera no RT (lido pela nova instrucao)
		last = histInstrucoes.size() - 1;        
		index = procuraInstrucao(histInstrucoes[last].addressRt,last-1,5);
		if(index != -1){
		    dist = last - index; //calcula distancia entre as instrucoes
		    if(dist == 1) { //hazard no pipeline de 5 estagios
		        dataHazards[0]++;
		        bubble[0] = true;
			bubbles5++;	
			gravaInstrucao("R-type",5,-1,-1,-1); //Insere nops no pipe-line
		    }
		}	
	}
        //hazard no pipeline de 8 estagios
	//procura por load que escrevera no RS (lido pela nova instrucao)
	last = histInstrucoes8.size() - 1;
	index = procuraInstrucao(histInstrucoes8[last].addressRs,last-1,8);
        if(index != -1){         
	    dist = last - index; //calcula distancia entre as instrucoes
            if(dist <= 2) { 
                dataHazards[1]++;
                bubble[1] = true;
                for(int i = dist; i <= 2; i++){
		   gravaInstrucao("R-type",8,-1,-1,-1); //Insere nops no pipe-line
		   bubbles8++;	
		}
            }
        }else{//procura por load que escrevera no RT (lido pela nova instrucao)
		last = histInstrucoes8.size() - 1;
		index = procuraInstrucao(histInstrucoes8[last].addressRt,last-1,8);
		if(index != -1){
		    dist = last - index; //calcula distancia entre as instrucoes
		    if(dist <= 2) { 
		        dataHazards[1]++;
		        bubble[1] = true;
		        for(int i = dist; i <= 2; i++){
			   gravaInstrucao("R-type",8,-1,-1,-1); //Insere nops no pipe-line
			   bubbles8++;	
			}
		    }
		}
	}

        //hazard no pipeline de 13 estagios
	//procura por load que escrevera no RS (lido pela nova instrucao)
	last = histInstrucoes14.size() - 1;
	index = procuraInstrucao(histInstrucoes14[last].addressRs,last-1,14);
        if(index != -1){         
	    dist = last - index; //calcula distancia entre as instrucoes
            if(dist <= 3) { 
                dataHazards[2]++;
                bubble[2] = true;
                for(int i = dist; i <= 3; i++){
		   gravaInstrucao("R-type",14,-1,-1,-1); //Insere nops no pipe-line
		   bubbles14++;	
		}
            }
        }else{//procura por load que escrevera no RT (lido pela nova instrucao)
		last = histInstrucoes14.size() - 1;
		index = procuraInstrucao(histInstrucoes14[last].addressRt,last-1,14);
		if(index != -1){
		    dist = last - index; //calcula distancia entre as instrucoes
		    if(dist <= 3) { 
		        dataHazards[2]++;
		        bubble[2] = true;
		        for(int i = dist; i <= 3; i++){
			   gravaInstrucao("R-type",14,-1,-1,-1); //Insere nops no pipe-line
			   bubbles14++;	
			}
		    }
		}
	}
  }
};


void checkSuperScalar(string instr)
{

 int last = histInstrucoes.size() - 1;
  if(last < 1){ //caso historico so tenha 1 instrucao
     return;
  }
	if(!parallel)
	{
	//Caso a instrução não esteja sendo rodada em paralelo
		if(!bubble[2])
		{
		
			//Caso não tenha nenhum hazard de dados para a instrução 
			if(strcmp(instr.c_str(),lastInstruction.c_str()))
			{
			//Caso as instru��es sejam diferentes vamos verificar e a instru��o anterior n�o � jump
			if(strcmp(lastInstruction.c_str(),"jump")){
			parallel = true;
			instrucoesParalelas=instrucoesParalelas+1;
			};
			
			
			}else if(!strcmp(instr.c_str(),lastInstruction.c_str()) && !strcmp(instr.c_str(),"alu")){
						//Caso a instru��o atual seja da alu e a anterior tamb�m, elas podem ser feitas em parlelo	
			parallel = true;
			instrucoesParalelas=instrucoesParalelas+1;
			};
			
		}
	}else{

	parallel = false;
	};
}

void secondPTaken() {
  if(secondState == takenOne) {
    secondState=takenZero;
  } 
  else if (secondState==notTakenZero) {
    secondState=takenOne;
    secondBranchErrors++;
  }
  else if (secondState==notTakenOne) {
    secondState=notTakenZero;
    secondBranchErrors++;
  }
}

void secondPNotTaken() {
  if(secondState == takenOne) {
    secondState=notTakenZero;
    secondBranchErrors++;
  } 
  else if (secondState==notTakenZero) {
    secondState=notTakenOne;
  }
  else if (secondState==takenZero) {
    secondState=takenOne;
    secondBranchErrors++;
  }
}

void firstPTaken() {
  if (firstState==notTaken) {
    firstState=taken;
    firstBranchErrors++;
  }
}

void firstPNotTaken() {
  if (firstState==taken) {
    firstState=notTaken;
    firstBranchErrors++;
  }
}


/*====================================================================================================
Fim
======================================================================================================*/
bool first = true;

//!Generic instruction behavior method.
void ac_behavior( instruction )
{

  dbg_printf("----- PC=%#x ----- %lld\n", (int) ac_pc, ac_instr_counter);
  trace << "i " << std::hex << ac_pc << " " << 4 << std::endl;
  //  dbg_printf("----- PC=%#x NPC=%#x ----- %lld\n", (int) ac_pc, (int)npc, ac_instr_counter);

#ifndef NO_NEED_PC_UPDATE
  ac_pc = npc;
  npc = ac_pc + 4;
  numeroInstrucoes+=1;
#endif

};

//! Instruction Format behavior methods.
void ac_behavior( Type_R ){


  }
void ac_behavior( Type_I ){

}
void ac_behavior( Type_J ){

}

//!Behavior called before starting simulation
void ac_behavior(begin)
{
  dbg_printf("@@@ begin behavior @@@\n");
  RB[0] = 0;
  npc = ac_pc + 4;

  // Is is not required by the architecture, but makes debug really easier
  for (int regNum = 0; regNum < 32; regNum ++)
    RB[regNum] = 0;
  hi = 0;
  lo = 0;

  RB[29] =  AC_RAM_END - 1024 - processors_started++ * DEFAULT_STACK_SIZE;

  trace.open("/tmp/trace.din");

  numeroInstrucoes=0;
  instrucoesParalelas = 0;
  parallel =false;

  dataHazards[0] = 0; //contador do pipeline de 5 estagios
  dataHazards[1] = 0; //contador do pipeline de 8 estagios
  dataHazards[2] = 0; //contador do pipeline de 14 estagios

  bubble[0] = false; //checador do pipeline de 5 estagios
  bubble[1] = false; //checador do pipeline de 8 estagios
  bubble[2] = false; //checador do pipeline de 14 estagios
}

//!Behavior called after finishing simulation
void ac_behavior(end)
{
  dbg_printf("@@@ end behavior @@@\n");
  
  trace.close();

  printf("Instrucoes : %d\n", numeroInstrucoes);
  printf("Primeira politica: braches errados: %u\n", firstBranchErrors);
  printf("Segunda politica: braches errados: %u\n", secondBranchErrors);


  printf("\nInstrucoes:\nBranch: %d\n", instTipoBranch);
  printf("Load: %d\n", instTipoLoad);
  printf("Store: %d\n", instTipoStore);
  printf("I-type (demais): %d\n", instTipoI);  
  printf("R-type: %d\n", instTipoR);
  printf("J-type: %d\n", instTipoJ);

  printf("\nData hazards no pipeline de 5 estagios: %d\n", dataHazards[0]);
  printf("\nBubbles inseridas no pipeline de 5 estagios: %d\n", bubbles5);
  printf("Instrucoes executadas: PP:%u\tSP:%u\n", numeroInstrucoes+firstBranchErrors*branchPenalty5stages ,numeroInstrucoes+secondBranchErrors*branchPenalty5stages);
  printf("Data hazards no pipeline de 8 estagios: %d\n", dataHazards[1]);
  printf("\nBubbles inseridas no pipeline de 8 estagios: %d\n", bubbles8);
  printf("Instrucoes executadas: PP:%u\tSP:%u\n", numeroInstrucoes+firstBranchErrors*branchPenalty8stages ,numeroInstrucoes+secondBranchErrors*branchPenalty8stages);
  printf("Data hazards no pipeline de 14 estagios: %d\n", dataHazards[2]);
  printf("\nBubbles inseridas no pipeline de 14 estagios: %d\n", bubbles14);
  printf("Instrucoes executadas: PP:%u\tSP:%u\n", numeroInstrucoes+firstBranchErrors*branchPenalty13stages ,numeroInstrucoes+secondBranchErrors*branchPenalty13stages);
  printf("Instrucoes em Paralelo : %d\n", instrucoesParalelas);
}


//!Instruction lb behavior method.
void ac_behavior( lb )
{
	
  char byte;
  unsigned address, offset;

  dbg_printf("lb r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
  checkSuperScalar("ls");
  lastInstruction = "ls";


  address = RB[rs] + imm;
  offset = address & 3;
  byte = (DM.read(address & ~3) >> ((3 - offset) * 8)) & 0xFF;
  
  trace << "r " << std::hex << (address & ~3) << " " << 1 << std::endl;
  
  RB[rt] = (ac_Sword)byte ;

  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoLoad++;
  gravaInstrucao("load",1,RB[rs],RB[rt],address);
  checaHazard();
};

//!Instruction lbu behavior method.
void ac_behavior( lbu )
{
  unsigned char byte;
  unsigned address, offset;

  dbg_printf("lbu r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
  
  address = RB[rs] + imm;
  offset = address & 3;
  byte = (DM.read(address & ~3) >> ((3 - offset) * 8)) & 0xFF;
  
  trace << "r " << std::hex << (address & ~3) << " " << 1 << std::endl;
  
  RB[rt] = byte;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoLoad++;
  gravaInstrucao("load",1,RB[rs],RB[rt],address);
  checaHazard();
    checkSuperScalar("ls");
    lastInstruction = "ls";
};

//!Instruction lh behavior method.
void ac_behavior( lh )
{

  short int half;
  unsigned address, offset;

  dbg_printf("lh r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);

  address = RB[rs]+ imm;
  offset = (address & 3) >> 1;
  half = (DM.read(address & ~3) >> (1 - offset) * 16) & 0xFFFF;

  trace << "r " << std::hex << (address & ~3) << " " << 2 << std::endl;

  RB[rt] = (ac_Sword) half;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoLoad++;
  gravaInstrucao("load",1,RB[rs],RB[rt],address);
  checaHazard();
      checkSuperScalar("ls");
    lastInstruction = "ls";
};

//!Instruction lhu behavior method.
void ac_behavior( lhu )
{

  unsigned short int  half;
  unsigned address, offset;

  dbg_printf("lhu r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
    
  address = RB[rs]+ imm;
  offset = (address & 3) >> 1;
  half = (DM.read(address & ~3) >> (1 - offset) * 16) & 0xFFFF;
  
  trace << "r " << std::hex << (address & ~3) << " " << 2 << std::endl;
  
  RB[rt] = half;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoLoad++;
  gravaInstrucao("load",1,RB[rs],RB[rt],address);
  checaHazard();
  checkSuperScalar("ls");
    lastInstruction = "ls";
};

//!Instruction lw behavior method.
void ac_behavior( lw )
{

  dbg_printf("lw r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);

  RB[rt] = DM.read(RB[rs]+ imm);

  trace << "r " << std::hex << RB[rs]+ imm << " " << 4 << std::endl;
  
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoLoad++;
  gravaInstrucao("load",1,RB[rs],RB[rt],RB[rs]+ imm);
  checaHazard();
        checkSuperScalar("ls");
    lastInstruction = "ls";
};

//!Instruction lwl behavior method.
void ac_behavior( lwl )
{

  dbg_printf("lwl r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);

  unsigned int addr, offset;
  ac_Uword data;

  addr = RB[rs] + imm;
  offset = (addr & 0x3) * 8;
  data = DM.read(addr & 0xFFFFFFFC);

 if (offset > 0)
    trace << "r " << std::hex << (addr & 0xFFFFFFFC) << " " << offset/8 << std::endl;
    
  data <<= offset;
  data |= RB[rt] & ((1<<offset)-1);
  RB[rt] = data;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoLoad++;
  gravaInstrucao("load",1,RB[rs],RB[rt],addr);
  checaHazard();
          checkSuperScalar("ls");
    lastInstruction = "ls";
};

//!Instruction lwr behavior method.
void ac_behavior( lwr )
{

  dbg_printf("lwr r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
 
  unsigned int addr, offset;
  ac_Uword data;

  addr = RB[rs] + imm;
  offset = (3 - (addr & 0x3)) * 8;
  data = DM.read(addr & 0xFFFFFFFC);
  
  if (offset > 0)
    trace << "r " << std::hex << (addr & 0xFFFFFFFC) << " " << offset/8 << std::endl;

  data >>= offset;
  data |= RB[rt] & (0xFFFFFFFF << (32-offset));
  RB[rt] = data;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoLoad++;
  gravaInstrucao("load",1,RB[rs],RB[rt],addr);
  checaHazard();
         checkSuperScalar("ls");
    lastInstruction = "ls";
};

//!Instruction sb behavior method.
void ac_behavior( sb )
{
  unsigned char byte;
  unsigned address, offset_ammount;
  ac_word data;

  dbg_printf("sb r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);

  address = RB[rs] + imm;
  offset_ammount = (3 - (address & 3)) * 8;
  byte = RB[rt] & 0xFF;
  data = DM.read(address & ~3) & ~(0xFF << offset_ammount) | (byte << offset_ammount);
  DM.write(address & ~3, data);
  
  trace << "w " << std::hex << (address & ~3) << " " << 1 << std::endl;

  dbg_printf("Result = %#x\n", (int) byte);

  //Detecao de hazards
  instTipoStore++;
  gravaInstrucao("store",1,RB[rs],RB[rt],address);
  checaHazard();
          checkSuperScalar("ls");
  lastInstruction = "ls";
};

//!Instruction sh behavior method.
void ac_behavior( sh )
{
  unsigned short int half;
  unsigned address, offset_ammount;
  ac_word data;

  dbg_printf("sh r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);

  address = RB[rs] + imm;
  offset_ammount = (1 - ((address & 3) >> 1)) * 16;
  half = RB[rt] & 0xFFFF;
  data = DM.read(address & ~3) & ~(0xFFFF << offset_ammount) | (half << offset_ammount);
  DM.write(address & ~3, data);
  
  trace << "w " << std::hex << (address & ~3) << " " << 2 << std::endl;
  
  dbg_printf("Result = %#x\n", (int) half);

  //Detecao de hazards
  instTipoStore++;
  gravaInstrucao("store",1,RB[rs],RB[rt],address);
  checaHazard();
      checkSuperScalar("ls");
    lastInstruction = "ls";

};

//!Instruction sw behavior method.
void ac_behavior( sw )
{
  dbg_printf("sw r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);

  DM.write(RB[rs] + imm, RB[rt]);

  trace << "w " << std::hex << RB[rs] + imm << " " << 4 << std::endl;
  
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoStore++;
  gravaInstrucao("store",1,RB[rs],RB[rt],RB[rs] + imm);
  checaHazard();
   checkSuperScalar("ls");
    lastInstruction = "ls";
};

//!Instruction swl behavior method.
void ac_behavior( swl )
{
  dbg_printf("swl r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);

  unsigned int addr, offset;
  ac_Uword data;

  addr = RB[rs] + imm;
  offset = (addr & 0x3) * 8;
  data = RB[rt];
  data >>= offset;
  data |= DM.read(addr & 0xFFFFFFFC) & (0xFFFFFFFF << (32-offset));
  DM.write(addr & 0xFFFFFFFC, data);
  
  if (offset > 0)
    trace << "w " << std::hex << (addr & 0xFFFFFFFC) << " " << offset/8 << std::endl;
  
  dbg_printf("Result = %#x\n", data);

  //Detecao de hazards
  instTipoStore++;
  gravaInstrucao("store",1,RB[rs],RB[rt],addr);
  checaHazard();
   checkSuperScalar("ls");
    lastInstruction = "ls";
};

//!Instruction swr behavior method.
void ac_behavior( swr )
{
  dbg_printf("swr r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);

  unsigned int addr, offset;
  ac_Uword data;

  addr = RB[rs] + imm;
  offset = (3 - (addr & 0x3)) * 8;
  data = RB[rt];
  data <<= offset;
  data |= DM.read(addr & 0xFFFFFFFC) & ((1<<offset)-1);
  DM.write(addr & 0xFFFFFFFC, data);
  
  if (offset > 0)
    trace << "w " << std::hex << (addr & 0xFFFFFFFC) << " " << offset/8 << std::endl;
  
  dbg_printf("Result = %#x\n", data);

  //Detecao de hazards
  instTipoStore++;
  gravaInstrucao("store",1,RB[rs],RB[rt],addr);
  checaHazard();
   checkSuperScalar("ls");
    lastInstruction = "ls";
};

//!Instruction addi behavior method.
void ac_behavior( addi )
{
  dbg_printf("addi r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);

  RB[rt] = RB[rs] + imm;
  dbg_printf("Result = %#x\n", RB[rt]);
  //Test overflow
  if ( ((RB[rs] & 0x80000000) == (imm & 0x80000000)) &&
       ((imm & 0x80000000) != (RB[rt] & 0x80000000)) ) {
    fprintf(stderr, "EXCEPTION(addi): integer overflow.\n"); exit(EXIT_FAILURE);
  }

  //Detecao de hazards
  instTipoI++;

  gravaInstrucao("I-type",1,RB[rs],RB[rt],imm);
  checaHazard();
    checkSuperScalar("alu");
  lastInstruction = "alu";

};

//!Instruction addiu behavior method.
void ac_behavior( addiu )
{
  dbg_printf("addiu r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);

  RB[rt] = RB[rs] + imm;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoI++;
  gravaInstrucao("I-type",1,RB[rs],RB[rt],imm);
  checaHazard();
   checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction slti behavior method.
void ac_behavior( slti )
{
  dbg_printf("slti r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);

  // Set the RD if RS< IMM
  if( (ac_Sword) RB[rs] < (ac_Sword) imm )
    RB[rt] = 1;
  // Else reset RD
  else
    RB[rt] = 0;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoI++;
  gravaInstrucao("I-type",1,RB[rs],RB[rt],imm);
  checaHazard();
   checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction sltiu behavior method.
void ac_behavior( sltiu )
{
  dbg_printf("sltiu r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);

  // Set the RD if RS< IMM
  if( (ac_Uword) RB[rs] < (ac_Uword) imm )
    RB[rt] = 1;
  // Else reset RD
  else
    RB[rt] = 0;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoI++;
  gravaInstrucao("I-type",1,RB[rs],RB[rt],imm);
  checaHazard();
   checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction andi behavior method.
void ac_behavior( andi )
{
  dbg_printf("andi r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);

  RB[rt] = RB[rs] & (imm & 0xFFFF) ;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoI++;
  gravaInstrucao("I-type",1,RB[rs],RB[rt],imm);
  checaHazard();
   checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction ori behavior method.
void ac_behavior( ori )
{
  dbg_printf("ori r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);

  RB[rt] = RB[rs] | (imm & 0xFFFF) ;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoI++;
  gravaInstrucao("I-type",1,RB[rs],RB[rt],imm);
  checaHazard();
   checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction xori behavior method.
void ac_behavior( xori )
{
  dbg_printf("xori r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);

  RB[rt] = RB[rs] ^ (imm & 0xFFFF) ;
  dbg_printf("Result = %#x\n", RB[rt]);

  //Detecao de hazards
  instTipoI++;
  gravaInstrucao("I-type",1,RB[rs],RB[rt],imm);
  checaHazard();
   checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction lui behavior method.
void ac_behavior( lui )
{
  dbg_printf("lui r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);

  // Load a constant in the upper 16 bits of a register
  // To achieve the desired behaviour, the constant was shifted 16 bits left
  // and moved to the target register ( rt )
  RB[rt] = imm << 16;
  dbg_printf("Result = %#x\n", RB[rt]);


  //Detecao de hazards
  instTipoI++;
  gravaInstrucao("I-type",1,-1,RB[rt],imm);
  checaHazard();
    checkSuperScalar("ls");
  lastInstruction = "ls";

};

//!Instruction add behavior method.
void ac_behavior( add )
{
  dbg_printf("add r%d, r%d, r%d\n", rd, rs, rt);

  RB[rd] = RB[rs] + RB[rt];
  dbg_printf("Result = %#x\n", RB[rd]);
  //Test overflow
  if ( ((RB[rs] & 0x80000000) == (RB[rd] & 0x80000000)) &&
       ((RB[rd] & 0x80000000) != (RB[rt] & 0x80000000)) ) {
    fprintf(stderr, "EXCEPTION(add): integer overflow.\n"); exit(EXIT_FAILURE);
  }

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction addu behavior method.
void ac_behavior( addu )
{
  dbg_printf("addu r%d, r%d, r%d\n", rd, rs, rt);

  RB[rd] = RB[rs] + RB[rt];
  //cout << "  RS: " << (unsigned int)RB[rs] << " RT: " << (unsigned int)RB[rt] << endl;
  //cout << "  Result =  " <<  (unsigned int)RB[rd] <<endl;
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction sub behavior method.
void ac_behavior( sub )
{
  dbg_printf("sub r%d, r%d, r%d\n", rd, rs, rt);

  RB[rd] = RB[rs] - RB[rt];
  dbg_printf("Result = %#x\n", RB[rd]);
  //TODO: test integer overflow exception for sub

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction subu behavior method.
void ac_behavior( subu )
{
  dbg_printf("subu r%d, r%d, r%d\n", rd, rs, rt);

  RB[rd] = RB[rs] - RB[rt];
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction slt behavior method.
void ac_behavior( slt )
{
  dbg_printf("slt r%d, r%d, r%d\n", rd, rs, rt);

  // Set the RD if RS< RT
  if( (ac_Sword) RB[rs] < (ac_Sword) RB[rt] )
    RB[rd] = 1;
  // Else reset RD
  else
    RB[rd] = 0;
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction sltu behavior method.
void ac_behavior( sltu )
{
  dbg_printf("sltu r%d, r%d, r%d\n", rd, rs, rt);

  // Set the RD if RS < RT
  if( RB[rs] < RB[rt] )
    RB[rd] = 1;
  // Else reset RD
  else
    RB[rd] = 0;
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction instr_and behavior method.
void ac_behavior( instr_and )
{
  dbg_printf("instr_and r%d, r%d, r%d\n", rd, rs, rt);

  RB[rd] = RB[rs] & RB[rt];
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction instr_or behavior method.
void ac_behavior( instr_or )
{
  dbg_printf("instr_or r%d, r%d, r%d\n", rd, rs, rt);

  RB[rd] = RB[rs] | RB[rt];
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction instr_xor behavior method.
void ac_behavior( instr_xor )
{
  dbg_printf("instr_xor r%d, r%d, r%d\n", rd, rs, rt);

  RB[rd] = RB[rs] ^ RB[rt];
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction instr_nor behavior method.
void ac_behavior( instr_nor )
{
  dbg_printf("nor r%d, r%d, r%d\n", rd, rs, rt);

  RB[rd] = ~(RB[rs] | RB[rt]);
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction nop behavior method.
void ac_behavior( nop )
{
  dbg_printf("nop\n");

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,-1,-1,-1);
  checaHazard();
};

//!Instruction sll behavior method.
void ac_behavior( sll )
{
  dbg_printf("sll r%d, r%d, %d\n", rd, rs, shamt);

  RB[rd] = RB[rt] << shamt;
  dbg_printf("Result = %#x\n", RB[rd]);

   


  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,-1,RB[rt],RB[rd]);
  checaHazard();
    checkSuperScalar("alu");
  lastInstruction = "alu";

};

//!Instruction srl behavior method.
void ac_behavior( srl )
{
  dbg_printf("srl r%d, r%d, %d\n", rd, rs, shamt);

  RB[rd] = RB[rt] >> shamt;
  dbg_printf("Result = %#x\n", RB[rd]);

   


  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,-1,RB[rt],RB[rd]);
  checaHazard();
  checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction sra behavior method.
void ac_behavior( sra )
{
  dbg_printf("sra r%d, r%d, %d\n", rd, rs, shamt);

  RB[rd] = (ac_Sword) RB[rt] >> shamt;
  dbg_printf("Result = %#x\n", RB[rd]);


  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,-1,RB[rt],RB[rd]);
  checaHazard();
   checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction sllv behavior method.
void ac_behavior( sllv )
{
  dbg_printf("sllv r%d, r%d, r%d\n", rd, rt, rs);

  RB[rd] = RB[rt] << (RB[rs] & 0x1F);
  dbg_printf("Result = %#x\n", RB[rd]);
  

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction srlv behavior method.
void ac_behavior( srlv )
{
  dbg_printf("srlv r%d, r%d, r%d\n", rd, rt, rs);

  RB[rd] = RB[rt] >> (RB[rs] & 0x1F);
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction srav behavior method.
void ac_behavior( srav )
{
  dbg_printf("srav r%d, r%d, r%d\n", rd, rt, rs);
             lastInstruction = "srav";  
  RB[rd] = (ac_Sword) RB[rt] >> (RB[rs] & 0x1F);
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],RB[rd]);
  checaHazard();
     checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction mult behavior method.
void ac_behavior( mult )
{
  dbg_printf("mult r%d, r%d\n", rs, rt);

  long long result;
  int half_result;

  result = (ac_Sword) RB[rs];
  result *= (ac_Sword) RB[rt];

  half_result = (result & 0xFFFFFFFF);
  // Register LO receives 32 less significant bits
  lo = half_result;

  half_result = ((result >> 32) & 0xFFFFFFFF);
  // Register HI receives 32 most significant bits
  hi = half_result ;

  dbg_printf("Result = %#llx\n", result);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],hi); //Sempre escrito em ambos hi e lo????
  checaHazard();
   checkSuperScalar("mult");
  lastInstruction = "mult";
};

//!Instruction multu behavior method.
void ac_behavior( multu )
{
  dbg_printf("multu r%d, r%d\n", rs, rt);

  unsigned long long result;
  unsigned int half_result;

  result  = RB[rs];
  result *= RB[rt];

  half_result = (result & 0xFFFFFFFF);
  // Register LO receives 32 less significant bits
  lo = half_result;

  half_result = ((result>>32) & 0xFFFFFFFF);
  // Register HI receives 32 most significant bits
  hi = half_result ;

  dbg_printf("Result = %#llx\n", result);


  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],hi); //Sempre escrito em ambos hi e lo????
  checaHazard();
  checkSuperScalar("mult");
  lastInstruction = "mult";
};

//!Instruction div behavior method.
void ac_behavior( div )
{
  dbg_printf("div r%d, r%d\n", rs, rt);
               lastInstruction = "div";  
  // Register LO receives quotient
  lo = (ac_Sword) RB[rs] / (ac_Sword) RB[rt];
  // Register HI receives remainder
  hi = (ac_Sword) RB[rs] % (ac_Sword) RB[rt];


  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],hi); //Sempre escrito em ambos hi e lo????
  checaHazard();
checkSuperScalar("mult");
  lastInstruction = "mult";
};

//!Instruction divu behavior method.
void ac_behavior( divu )
{
  dbg_printf("divu r%d, r%d\n", rs, rt);

  // Register LO receives quotient
  lo = RB[rs] / RB[rt];
  // Register HI receives remainder
  hi = RB[rs] % RB[rt];
  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],RB[rt],hi); //Sempre escrito em ambos hi e lo????
  checaHazard();
    checkSuperScalar("mult");
  lastInstruction = "mult";
};

//!Instruction mfhi behavior method.
void ac_behavior( mfhi )
{
  dbg_printf("mfhi r%d\n", rd);
     lastInstruction = "mfhi";  
  RB[rd] = hi;
  dbg_printf("Result = %#x\n", RB[rd]);


  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,hi,-1,RB[rd]); 
  checaHazard(); 
    checkSuperScalar("mult");
  lastInstruction = "mult";
};

//!Instruction mthi behavior method.
void ac_behavior( mthi )
{
  dbg_printf("mthi r%d\n", rs);
       lastInstruction = "mthi";  
  hi = RB[rs];
  dbg_printf("Result = %#x\n", (unsigned int) hi);
  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],-1,hi); 
  checaHazard();
   checkSuperScalar("mult");
  lastInstruction = "mult";
};

//!Instruction mflo behavior method.
void ac_behavior( mflo )
{
  dbg_printf("mflo r%d\n", rd);
        lastInstruction = "mflo";   
  RB[rd] = lo;
  dbg_printf("Result = %#x\n", RB[rd]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,lo,-1,RB[rd]); 
  checaHazard();
    checkSuperScalar("mult");
  lastInstruction = "mult";
};

//!Instruction mtlo behavior method.
void ac_behavior( mtlo )
{
  dbg_printf("mtlo r%d\n", rs);
          lastInstruction = "mtlo";   
  lo = RB[rs];
  dbg_printf("Result = %#x\n", (unsigned int) lo);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],-1,lo); 
  checaHazard();
    checkSuperScalar("mult");
  lastInstruction = "mult";
};

//!Instruction j behavior method.
void ac_behavior( j )
{
  dbg_printf("j %d\n", addr);
            lastInstruction = "jump";   
  addr = addr << 2;
#ifndef NO_NEED_PC_UPDATE
  npc =  (ac_pc & 0xF0000000) | addr;
#endif
  dbg_printf("Target = %#x\n", (ac_pc & 0xF0000000) | addr );

  //Detecao de hazards
  instTipoJ++;
  gravaInstrucao("J-type",1,-1,-1,-1); 
  checaHazard();
};

//!Instruction jal behavior method.
void ac_behavior( jal )
{
  dbg_printf("jal %d\n", addr);
  lastInstruction = "jump";   
  // Save the value of PC + 8 (return address) in $ra ($31) and
  // jump to the address given by PC(31...28)||(addr<<2)
  // It must also flush the instructions that were loaded into the pipeline
  RB[Ra] = ac_pc+4; //ac_pc is pc+4, we need pc+8

  addr = addr << 2;
#ifndef NO_NEED_PC_UPDATE
  npc = (ac_pc & 0xF0000000) | addr;
#endif

  dbg_printf("Target = %#x\n", (ac_pc & 0xF0000000) | addr );
  dbg_printf("Return = %#x\n", ac_pc+4);

  //Detecao de hazards
  instTipoJ++;
  gravaInstrucao("J-type",1,-1,-1,-1); 
  checaHazard();
};

//!Instruction jr behavior method.
void ac_behavior( jr )
{
  dbg_printf("jr r%d\n", rs);
                lastInstruction = "jump";   
  // Jump to the address lsd on the register reg[RS]
  // It must also flush the instructions that were loaded into the pipeline
#ifndef NO_NEED_PC_UPDATE
  npc = RB[rs], 1;
#endif
  dbg_printf("Target = %#x\n", RB[rs]);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],-1,-1); 
  checaHazard();
};

//!Instruction jalr behavior method.
void ac_behavior( jalr )
{
  dbg_printf("jalr r%d, r%d\n", rd, rs);
                  lastInstruction = "jump";   
  // Save the value of PC + 8(return address) in rd and
  // jump to the address given by [rs]

#ifndef NO_NEED_PC_UPDATE
  npc = RB[rs], 1;
#endif
  dbg_printf("Target = %#x\n", RB[rs]);

  if( rd == 0 )  //If rd is not defined use default
    rd = Ra;
  RB[rd] = ac_pc+4;
  dbg_printf("Return = %#x\n", ac_pc+4);

  //Detecao de hazards
  instTipoR++;
  gravaInstrucao("R-type",1,RB[rs],-1,RB[rd]); 
  checaHazard();
};

//!Instruction beq behavior method.
void ac_behavior( beq )
{
  dbg_printf("beq r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
                    lastInstruction = "beq";   
  if( RB[rs] == RB[rt] ){
    firstPTaken();
    secondPTaken();
#ifndef NO_NEED_PC_UPDATE
    npc = ac_pc + (imm<<2);
#endif
    dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
  }
  else {
    firstPNotTaken();
    secondPNotTaken();
  }

  //Detecao de hazards
  instTipoBranch++;
  gravaInstrucao("branch",1,RB[rs],RB[rt],-1); 
  checaHazard();
   checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction bne behavior method.
void ac_behavior( bne )
{
  dbg_printf("bne r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);

  if( RB[rs] != RB[rt] ){
    firstPTaken();
    secondPTaken();
#ifndef NO_NEED_PC_UPDATE
    npc = ac_pc + (imm<<2);
#endif
    dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
  }
  else {
    firstPNotTaken();
    secondPNotTaken();
  }


  //Detecao de hazards
  instTipoBranch++;
  gravaInstrucao("branch",1,RB[rs],RB[rt],-1); 
  checaHazard();
  checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction blez behavior method.
void ac_behavior( blez )
{
  dbg_printf("blez r%d, %d\n", rs, imm & 0xFFFF);
   lastInstruction = "blez";   
  if( (RB[rs] == 0 ) || (RB[rs]&0x80000000 ) ){
    firstPTaken();
    secondPTaken();
#ifndef NO_NEED_PC_UPDATE
    npc = ac_pc + (imm<<2), 1;
#endif
    dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
  }
  else {
    firstPNotTaken();
    secondPNotTaken();
  }
 //Detecao de hazards
  instTipoBranch++;
  gravaInstrucao("branch",1,RB[rs],-1,-1); 
  checaHazard();
  checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction bgtz behavior method.
void ac_behavior( bgtz )
{
  dbg_printf("bgtz r%d, %d\n", rs, imm & 0xFFFF);
     lastInstruction = "bgtz";   
  if( !(RB[rs] & 0x80000000) && (RB[rs]!=0) ){
    firstPTaken();
    secondPTaken();
#ifndef NO_NEED_PC_UPDATE
    npc = ac_pc + (imm<<2);
#endif
    dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
  }
  else {
    firstPNotTaken();
    secondPNotTaken();
  }
  //Detecao de hazards
  instTipoBranch++;
  gravaInstrucao("branch",1,RB[rs],-1,-1); 
  checaHazard();
  checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction bltz behavior method.
void ac_behavior( bltz )
{
  dbg_printf("bltz r%d, %d\n", rs, imm & 0xFFFF);
   
  if( RB[rs] & 0x80000000 ){
    firstPTaken();
    secondPTaken();
#ifndef NO_NEED_PC_UPDATE
    npc = ac_pc + (imm<<2);
#endif
    dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
  }
  else {
    firstPNotTaken();
    secondPNotTaken();
  }


  //Detecao de hazards
  instTipoBranch++;
  gravaInstrucao("branch",1,RB[rs],-1,-1); 
  checaHazard();
    checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction bgez behavior method.
void ac_behavior( bgez )
{
  dbg_printf("bgez r%d, %d\n", rs, imm & 0xFFFF);
      lastInstruction = "bgez";   
  if( !(RB[rs] & 0x80000000) ){
    firstPTaken();
    secondPTaken();
#ifndef NO_NEED_PC_UPDATE
    npc = ac_pc + (imm<<2);
#endif
    dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
  }
  else {
    firstPNotTaken();
    secondPNotTaken();
  }


  //Detecao de hazards
  instTipoBranch++;
  gravaInstrucao("branch",1,RB[rs],-1,-1); 
  checaHazard();
   checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction bltzal behavior method.
void ac_behavior( bltzal )
{
  dbg_printf("bltzal r%d, %d\n", rs, imm & 0xFFFF);
      lastInstruction = "bltzal";   
  RB[Ra] = ac_pc+4; //ac_pc is pc+4, we need pc+8
  if( RB[rs] & 0x80000000 ){
    firstPTaken();
    secondPTaken();
#ifndef NO_NEED_PC_UPDATE
    npc = ac_pc + (imm<<2);
#endif
    dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
  }
  else {
    firstPNotTaken();
    secondPNotTaken();
  }
  dbg_printf("Return = %#x\n", ac_pc+4);

  //Detecao de hazards
  instTipoBranch++;
  gravaInstrucao("branch",1,RB[rs],-1,-1); 
  checaHazard();
   checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction bgezal behavior method.
void ac_behavior( bgezal )
{
  dbg_printf("bgezal r%d, %d\n", rs, imm & 0xFFFF);
      lastInstruction = "bgezal";   
  RB[Ra] = ac_pc+4; //ac_pc is pc+4, we need pc+8
  if( !(RB[rs] & 0x80000000) ){
    firstPTaken();
    secondPTaken();
#ifndef NO_NEED_PC_UPDATE
    npc = ac_pc + (imm<<2);
#endif
    dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
  }
  else {
    firstPNotTaken();
    secondPNotTaken();
  }
  dbg_printf("Return = %#x\n", ac_pc+4);

  //Detecao de hazards
  instTipoBranch++;
  gravaInstrucao("branch",1,RB[rs],-1,-1); 
  checaHazard();
  checkSuperScalar("alu");
  lastInstruction = "alu";
};

//!Instruction sys_call behavior method.
void ac_behavior( sys_call )
{
  dbg_printf("syscall\n");
  stop();
}

//!Instruction instr_break behavior method.
void ac_behavior( instr_break )
{
  fprintf(stderr, "instr_break behavior not implemented.\n");
  exit(EXIT_FAILURE);
}
/*====================================================================================================
Instruções para Projeto 2 MC723
======================================================================================================*/
